package my.com.katyworld.iBuddyCommission.calculation;

import my.com.katyworld.iBuddyCommission.services.TreeLookupService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class AdvisorCalculationInvalid {

    private static final Logger LOG = Logger.getLogger(AdvisorCalculationInvalid.class.getName());

    private static BigDecimal calculateAdvisorPersonalProdComm(BigDecimal pprod, BigDecimal[][] advisorPersonalCommRates) {
        BigDecimal HUNDRED = new BigDecimal(100);
        BigDecimal copy = new BigDecimal(pprod.toString());
        BigDecimal res = new BigDecimal(0);
        int i = 0;
        while (i < advisorPersonalCommRates.length &&
                copy.compareTo(advisorPersonalCommRates[i][0]) >= 0) {
            res = res.add(advisorPersonalCommRates[i][0].multiply(advisorPersonalCommRates[i][1])
                    .divide(HUNDRED, RoundingMode.HALF_UP));
            copy = copy.subtract(advisorPersonalCommRates[i][0]);
            i++;
        }

        if (i < advisorPersonalCommRates.length) {
            res = res.add(copy.multiply(advisorPersonalCommRates[i][1]).divide(HUNDRED, RoundingMode.HALF_UP));
        }
        return res;
    }

    private static Map<Integer, List<String>> getSubAgentsAtLevels(String agentCode, Map<String,
            List<String>> agentCodeMap, Collection<Integer> lvls) {
        Map<Integer, List<String>> res = new HashMap<>();

        Queue<String> q = new LinkedList<>();
        q.add(agentCode);
        int i = 0;
        int frontierLength;
        while (!q.isEmpty()) {
            boolean currentDepthInLevels = lvls.contains(i);
            if (currentDepthInLevels) res.put(i, new ArrayList<>());
            frontierLength = q.size();
            for (int j = 0; j < frontierLength; j++) {
                String s = q.poll();
                if (currentDepthInLevels) res.get(i).add(s);
                if (agentCodeMap.containsKey(s)) {
                    q.addAll(agentCodeMap.get(s));
                }
            }
            i++;
        }
        return res;
    }

    /**
     * @param agentCode
     * @param agentInfoMap
     * @param agentCodeMap
     * @param advisorPersonalCommRates
     * @param advisorSubCommRates
     */
    public static void calculateAdvisorComm(String agentCode,
                                            Map<String, TreeLookupService.AgentInfo> agentInfoMap,
                                            Map<String, List<String>> agentCodeMap,
                                            BigDecimal[][] advisorPersonalCommRates,
                                            Map<Integer, BigDecimal> advisorSubCommRates) {
        BigDecimal HUNDRED = new BigDecimal(100);
        TreeLookupService.AdvisorAgentInfo advisorAgentInfo = (TreeLookupService.AdvisorAgentInfo) agentInfoMap.get(agentCode);
        int years = advisorAgentInfo.getPersonalProduction().size();
        for (int i = 0; i < years; i++) {
            // calculate personal production commission
            BigDecimal totalAdvisorCommThisYear = new BigDecimal(0);
            BigDecimal pprod = advisorAgentInfo.getPersonalProduction().get(i);
            BigDecimal pprodComm = calculateAdvisorPersonalProdComm(pprod, advisorPersonalCommRates);

            totalAdvisorCommThisYear = totalAdvisorCommThisYear.add(pprodComm);

            advisorAgentInfo.getPersonalProdCommission().add(pprodComm);
            advisorAgentInfo.getAdvisorCommBreakdown().add(new HashMap<>());

            Map<Integer, List<String>> subAgents = getSubAgentsAtLevels(agentCode, agentCodeMap,
                    advisorSubCommRates.keySet());
            LOG.info("subAgents of " + agentCode + " are " + subAgents.toString());
            for (int lvl : subAgents.keySet()) {
                BigDecimal commPercentageAtThisLevel = advisorSubCommRates.get(lvl);
                LOG.info("finding productions for year " + String.valueOf(i) + " level " + lvl + " advisor " + agentCode);
                // sum up all of their personal sales amounts
                List<String> subAgentsAtThisLevel = subAgents.get(lvl);
                Stream<BigDecimal> theirProductions = subAgentsAtThisLevel.stream().map(agt ->
                        agentInfoMap.get(agt).getPersonalProduction().get(i));
                BigDecimal totalProductionAtThisLevel = theirProductions.reduce(BigDecimal::add).get();
                LOG.info("production " + totalProductionAtThisLevel.toString());
                advisorAgentInfo.getAdvisorCommBreakdown().get(i).put("lvl" + String.valueOf(lvl), totalProductionAtThisLevel);

                BigDecimal totalCommissionAtThisLevel = totalProductionAtThisLevel.multiply(commPercentageAtThisLevel)
                        .divide(HUNDRED);
                totalAdvisorCommThisYear = totalAdvisorCommThisYear.add(totalCommissionAtThisLevel);
            }

            advisorAgentInfo.getAdvisorComm().add(totalAdvisorCommThisYear);
            break;
        }

    }
}
