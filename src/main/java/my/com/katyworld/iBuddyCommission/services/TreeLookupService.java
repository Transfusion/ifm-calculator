package my.com.katyworld.iBuddyCommission.services;

import my.com.katyworld.iBuddyCommission.dtos.TreeVizDTO;
import my.com.katyworld.iBuddyCommission.entities.Agent;
import my.com.katyworld.iBuddyCommission.repositories.AgentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Reads the agent tree into memory at the start of the program
 */
@Service
public class TreeLookupService {
    public static class AgentInfo {
        public String getAgentCode() {
            return agentCode;
        }

        private final String agentCode;

        public String getAgentType() {
            return agentType;
        }

        private final String agentType;

        public AgentInfo(String agentCode, String agentType) {
            this.agentCode = agentCode;
            this.agentType = agentType;
        }

        public List<BigDecimal> getAFYP() {
            return AFYP;
        }

        public void setAFYP(List<BigDecimal> AFYP) {
            this.AFYP = AFYP;
        }

        private List<BigDecimal> AFYP = new ArrayList<>();

        public List<BigDecimal> getPersonalProduction() {
            return personalProduction;
        }

        public void setPersonalProduction(List<BigDecimal> personalProduction) {
            this.personalProduction = personalProduction;
            // reinitialize
            this.AFYP = new ArrayList<>();
            this.personalProdCommission = new ArrayList<>();
        }

        private List<BigDecimal> personalProduction = new ArrayList<>();

        public List<BigDecimal> getPersonalProdCommission() {
            return personalProdCommission;
        }

        public void setPersonalProdCommission(List<BigDecimal> personalProdCommission) {
            this.personalProdCommission = personalProdCommission;
        }

        private List<BigDecimal> personalProdCommission = new ArrayList<>();
    }

    public static class AdvisorAgentInfo extends AgentInfo {

        public AdvisorAgentInfo(String agentCode, String agentType) {
            super(agentCode, agentType);
        }


        public List<BigDecimal> getAdvisorComm() {
            return advisorComm;
        }

        public void setAdvisorComm(List<BigDecimal> advisorComm) {
            this.advisorComm = advisorComm;
        }

        private List<BigDecimal> advisorComm = new ArrayList<>();

        @Override
        public void setPersonalProduction(List<BigDecimal> personalProduction) {
            super.setPersonalProduction(personalProduction);
            this.advisorComm = new ArrayList<>();
            this.advisorCommBreakdown = new ArrayList<>();
        }

        public List<Map<String, BigDecimal>> getAdvisorCommBreakdown() {
            return advisorCommBreakdown;
        }

        public void setAdvisorCommBreakdown(List<Map<String, BigDecimal>> advisorCommBreakdown) {
            this.advisorCommBreakdown = advisorCommBreakdown;
        }

        private List<Map<String, BigDecimal>> advisorCommBreakdown;
    }

    private static final Logger LOG = Logger.getLogger(TreeLookupService.class.getName());

    @Autowired
    public AgentRepository agentRepository;

    public TreeLookupService() {
//        this.agentTree = new DefaultTreeModel();
    }

    //    @Override
    public List<Agent> findAll() {
        List<Agent> agents = agentRepository.findAll();
        return agents;
    }

    /**
     * Birds eye view of the REPORTING tree(s)
     *
     * @return List of trees
     */
    public List<TreeVizDTO> getTreeVizDTOs(int year, int month) {
        ReportingTree reportingTree = getReportingTree(year, month);
        List<TreeVizDTO> treeVizDTOs = new ArrayList<>();
        for (String root : reportingTree.treeRoots) {
            treeVizDTOs.add(new TreeVizDTO(root, reportingTree));
        }
        return treeVizDTOs;
    }

    public List<TreeVizDTO> getAdvisorCommTreeVizDTOs(String agentCode,
                                                      int year, int month) {
        ReportingTree reportingTree = getReportingTree(year, month);
        if (!reportingTree.agentInfoMap.containsKey(agentCode))
            throw new IllegalArgumentException("Agent code doesn't exist in Agent table.");


        return null;
    }

    /**
     * Fetches the entire reporting tree for this year/month combo
     * cached for all the inputs (year/month combo) unconditionally
     * <p>
     * If there are multiple reporting trees for this year/month combo
     * then fetch the latest
     *
     * @param year  if null, will fetch for the current year/month combo
     * @param month current month
     * @return Reporting tree
     */
//    @Cacheable(value = "reportingTree")
    public ReportingTree getReportingTree(Integer year, Integer month) {
        Map<String, List<String>> agentCodeMap = new HashMap<>();
        Map<String, Agent> agentInfoMap = new HashMap<>();
        List<String> treeRoots = new ArrayList<>();


        List<Agent> agents = agentRepository.findAll();

        for (Agent agt : agents) {
            String ownCode = agt.AgentCode;
            agentInfoMap.put(ownCode, agt);

            String superiorCode = agt.DirectAgtCode;
            if (superiorCode == null) {
                treeRoots.add(ownCode);
            }
            if (!agentCodeMap.containsKey(superiorCode)) {
                agentCodeMap.put(superiorCode, new ArrayList<>());
            }
            agentCodeMap.get(superiorCode).add(ownCode);
        }
        LOG.log(Level.INFO, "Getting Reporting Tree done, tree roots are: " + String.join(" ", treeRoots));

        return new ReportingTree(agentCodeMap, agentInfoMap, treeRoots);
    }

    /**
     * Entire reporting tree
     */
    public static class ReportingTree {
        /**
         * Agents who do not report to anyone
         */
        public final List<String> treeRoots;
        /**
         * Adjacency list of agent code to children
         */
        public final Map<String, List<String>> agentCodeMap;
        /**
         * agentCode to the actual Agent object.
         */
        public final Map<String, Agent> agentInfoMap;

        public ReportingTree(Map<String, List<String>> agentCodeMap,
                             Map<String, Agent> agentInfoMap,
                             List<String> treeRoots) {
            this.agentCodeMap = agentCodeMap;
            this.agentInfoMap = agentInfoMap;
            this.treeRoots = treeRoots;
        }
    }

    @PostConstruct
    public void init() {
    }
}
