package my.com.katyworld.iBuddyCommission.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import my.com.katyworld.iBuddyCommission.entities.IFMConfigEntry;
import my.com.katyworld.iBuddyCommission.repositories.IFMConfigRepository;
import my.com.katyworld.iBuddyCommission.util.Constants;
import my.com.katyworld.iBuddyCommission.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Logger;

@Service
public class IFMConfigService {
    private static final Logger LOG = Logger.getLogger(IFMConfigService.class.getName());

    static class AdvisorCommissionPersonalEntry {
        public int getPos() {
            return pos;
        }

        public void setPos(int pos) {
            this.pos = pos;
        }

        public BigDecimal getS() {
            return s;
        }

        public void setS(BigDecimal s) {
            this.s = s;
        }

        public BigDecimal getC() {
            return c;
        }

        public void setC(BigDecimal c) {
            this.c = c;
        }

        private int pos;
        private BigDecimal s;
        private BigDecimal c;
    }

    private static class AdvisorCommissionSubEntry {
        private int lvl;

        public int getLvl() {
            return lvl;
        }

        public void setLvl(int lvl) {
            this.lvl = lvl;
        }

        public BigDecimal getC() {
            return c;
        }

        public void setC(BigDecimal c) {
            this.c = c;
        }

        private BigDecimal c;
    }

    @Autowired
    public IFMConfigRepository ifmConfigRepository;

    private List<AdvisorCommissionSubEntry> getAdvisorSubCommissionEntries(int year, int month) throws IOException {
        List<IFMConfigEntry> entries = ifmConfigRepository.getLatestIFMConfigEntryByK(Constants.IFMConfigIDs.ADVISOR_COMMISSION, year + "-" + String.format("%02d", month) + "-" + Util.getLastDayOfMonth(year, month));

        if (entries.size() == 0)
            throw new IllegalStateException("advisorCommission entry isn't available");


        IFMConfigEntry entry = entries.get(0);
        JsonNode j = entry.getJsonNodeValue().get("subAdvisors");
        LOG.info("Read subAdvisors as json " + j.toString());

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectReader reader = objectMapper.readerFor(new TypeReference<List<AdvisorCommissionSubEntry>>() {
        });

        List<AdvisorCommissionSubEntry> list = reader.readValue(j);
        return list;
    }

    public Map<Integer, BigDecimal> getAdvisorSubCommission(int year, int month) throws IllegalStateException, IOException {
        List<AdvisorCommissionSubEntry> list = getAdvisorSubCommissionEntries(year, month);
        Map<Integer, BigDecimal> res = new HashMap<>();
        for (AdvisorCommissionSubEntry e : list)
            res.put(e.getLvl(), e.getC());

        return res;
    }

    public Map<Integer, BigDecimal> getAdvisorSubCommission(int year, int month, int maxLevel) throws IllegalStateException, IOException {
        List<AdvisorCommissionSubEntry> list = getAdvisorSubCommissionEntries(year, month);
        Map<Integer, BigDecimal> res = new HashMap<>();
        for (AdvisorCommissionSubEntry e : list) {
            if (e.getLvl() <= maxLevel)
                res.put(e.getLvl(), e.getC());
        }

        return res;
    }

    /* public BigDecimal[][] getAdvisorPersonalCommission() throws IOException, IllegalStateException {
        BigDecimal[][] res;

        Optional<IFMConfigEntry> _entry = ifmConfigRepository
                .findById(Constants.IFMConfigIDs.ADVISOR_COMMISSION);

        if (_entry.isEmpty())
            throw new IllegalStateException("advisorCommission entry isn't present in IFMConfig table");
        IFMConfigEntry entry = _entry.get();

        JsonNode j = entry.getJsonNodeValue().get("personal");

        LOG.info("Read personal as json " + j.toString());

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectReader reader = objectMapper.readerFor(new TypeReference<List<AdvisorCommissionPersonalEntry>>() {
        });

        List<AdvisorCommissionPersonalEntry> list = reader.readValue(j);
        list.sort(Comparator.comparingInt(o -> o.pos));

        res = new BigDecimal[list.size()][];
        for (int i = 0; i < list.size(); i++) {
            res[i] = new BigDecimal[]{list.get(i).s, list.get(i).c};
        }

        return res;
    } */


    /**
     * The particular personal commission rates in that year/month combo
     *
     * @param year
     * @param month
     * @return
     * @throws IOException
     * @throws IllegalStateException
     */
    public List<AdvisorCommissionPersonalEntry> getAdvisorPersonalCommission(int year, int month) throws IOException, IllegalStateException {

        /* IFMConfigEntry entry = ifmConfigRepository
                .findIFMConfigEntryByK(Constants.IFMConfigIDs.ADVISOR_COMMISSION);*/
        List<IFMConfigEntry> entries = ifmConfigRepository.getLatestIFMConfigEntryByK(Constants.IFMConfigIDs.ADVISOR_COMMISSION, year + "-" + String.format("%02d", month) + "-" + Util.getLastDayOfMonth(year, month));

        if (entries.size() == 0)
            throw new IllegalStateException("advisorCommission entry isn't available");
        IFMConfigEntry entry = entries.get(0);

        JsonNode j = entry.getJsonNodeValue().get("personal");

        LOG.info("Read personal as json " + j.toString());

        ObjectMapper objectMapper = new ObjectMapper();
        ObjectReader reader = objectMapper.readerFor(new TypeReference<List<AdvisorCommissionPersonalEntry>>() {
        });

        List<AdvisorCommissionPersonalEntry> list = reader.readValue(j);
        list.sort(Comparator.comparingInt(o -> o.pos));

        return list;
    }

    public BigDecimal getLevelThreshold(int year, int month) {
        List<IFMConfigEntry> entries = ifmConfigRepository.getLatestIFMConfigEntryByK(Constants.IFMConfigIDs.ADVISOR_COMMISSION, year + "-" + String.format("%02d", month) + "-" + Util.getLastDayOfMonth(year, month));

        if (entries.size() == 0)
            throw new IllegalStateException("advisorCommission entry isn't available");
        IFMConfigEntry entry = entries.get(0);

        JsonNode j = entry.getJsonNodeValue().get("levelThreshold");
        return j.decimalValue();
    }
}
