package my.com.katyworld.iBuddyCommission.services;

import my.com.katyworld.iBuddyCommission.entities.AdvisorProcessingCutoffEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;


interface LevelPopulator {
    public void populate(String _agentCode,
                         int _depth);
}

interface SubtreeIsLineChecker {
    public boolean check(String _agentCode);
}

@Service
public class AdvisorCalculationService {

    private static final Logger LOG = Logger.getLogger(AdvisorCalculationService.class.getName());

    @Autowired
    private IFMConfigService ifmConfigService;

    @Autowired
    private TreeLookupService treeLookupService;

    @Autowired
    private AgentCommissionService agentCommissionService;

    public static class LevelInfo {
        public final int level;
        public final Map<String, BigDecimal> subAdvisors;
        public final BigDecimal totalProd;
        public final BigDecimal commRate;
        public final BigDecimal comm;

        public LevelInfo(int level, Map<String, BigDecimal> subAdvisors, BigDecimal commRate) {
            BigDecimal HUNDRED = new BigDecimal(100);
            this.level = level;
            this.subAdvisors = subAdvisors;

            totalProd = subAdvisors.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);

            this.commRate = commRate;
            this.comm = totalProd.multiply(commRate).divide(HUNDRED, RoundingMode.HALF_UP);
        }
    }

    private static class AgentProdPair {
        public final String agentCode;
        public final BigDecimal prod;

        public AgentProdPair(String agentCode, BigDecimal prod) {
            this.agentCode = agentCode;
            this.prod = prod;
        }

        @Override
        public String toString() {
            return String.format("%s:%s", agentCode, prod.toString());
        }
    }

    public static class AdvisorProdCommPair {
        public final BigDecimal prod;

        public AdvisorProdCommPair(BigDecimal prod, BigDecimal comm) {
            this.prod = prod;
            this.comm = comm;
        }

        public final BigDecimal comm;
    }

    public static class AdvisorSalesComm {
        // nullable
        public final Map<Integer, LevelInfo> levelInfo;
        public final BigDecimal personalProd; // accumulated, no breakdown
        public final BigDecimal personalComm; // not the comm rate!
        public final BigDecimal TOTALCOMM; // sum of personalComm with levelInfo reduced

        AdvisorSalesComm(BigDecimal personalProd, BigDecimal personalComm,
                         Map<Integer, LevelInfo> levelInfo) {
            this.levelInfo = levelInfo;
            this.personalProd = personalProd;
            this.personalComm = personalComm;
            if (levelInfo != null) {
                this.TOTALCOMM = personalComm.add(levelInfo.values().stream().map(l -> l.comm).reduce(BigDecimal.ZERO, BigDecimal::add));
            } else {
                this.TOTALCOMM = personalComm;
            }
        }

    }

    public AdvisorSalesComm getAdvisorSalesComm(String agentCode, int year, int month) throws IOException {

        AdvisorProdCommPair x = getAdvisorProdComm(agentCode, year, month);
        BigDecimal personalProd = x.prod;
        BigDecimal personalComm = x.comm;
        if (eligibleForOverriding(agentCode, year, month)) {
            return new AdvisorSalesComm(personalProd, personalComm, getLevelInfo(agentCode, year, month));
        } else {
            return new AdvisorSalesComm(personalProd, personalComm, null);
        }
    }

    /**
     * Gets the prod and comm in the given year UP TO and including the given month
     *
     * @param agentCode
     * @param year
     * @param month
     * @return
     */
    public AdvisorProdCommPair getAdvisorProdComm(String agentCode, int year, int month) throws IOException, IllegalStateException {
        BigDecimal HUNDRED = new BigDecimal(100);

        BigDecimal personalProd = agentCommissionService.getAccumulatedPersonalProd(agentCode, year, month);

        // get the last instant in the given month
        LocalDate ld = LocalDate.of(year, month, 1);
        ld = ld.with(TemporalAdjusters.lastDayOfMonth());
        LocalDateTime ldt = LocalDateTime.of(ld, LocalTime.of(23, 59, 59));

        // if cutoffs are: 10K, 65K, 80K, 100K
        List<AdvisorProcessingCutoffEntry> cutoffEntries = agentCommissionService.getLastCutoffs(agentCode, java.sql.Timestamp.valueOf(ldt));

        List<BigDecimal> cutoffs = cutoffEntries.stream().map(e -> e.accumProd).collect(Collectors.toList());

        if (cutoffs.size() == 0) return new AdvisorProdCommPair(personalProd, BigDecimal.ZERO);

        cutoffs.add(0, BigDecimal.ZERO);

        Assert.state(personalProd.compareTo(cutoffs.get(cutoffs.size() - 1)) > -1, "Accumulated personal production calculated from the Cmp_Prod_Agt table cannot be smaller than the last processed value");

        // then find the differences
        // 10K, 55K, 15K, 20K
        List<BigDecimal> differences = new ArrayList<>();
        for (int i = 1; i < cutoffs.size(); i++)
            differences.add(cutoffs.get(i).subtract(cutoffs.get(i - 1)));
        cutoffs.remove(0);

        // already sorted by relative position
        List<IFMConfigService.AdvisorCommissionPersonalEntry> personalCommConfigEntries =
                ifmConfigService.getAdvisorPersonalCommission(year, month);

        List<BigDecimal> appliedCommissionRates = new ArrayList<>();
        // the 1st cutoff is always bound to have the lowest commission rate
        // because increasing commission will only take effect on the next payout
        appliedCommissionRates.add(personalCommConfigEntries.get(0).getC());

        int j = 0;

        for (int i = 0; i < personalCommConfigEntries.size(); i++) {
            LOG.info(String.format("%d %d", i, j));
            IFMConfigService.AdvisorCommissionPersonalEntry entry = personalCommConfigEntries.get(i);

            // while smaller than the entry; hence if the threshold is 60K and
            // the cutoff is exactly 60K it will take effect next process
            while (j < cutoffs.size() && cutoffs.get(j).compareTo(entry.getS()) < 0) {
                appliedCommissionRates.add(entry.getC());
                j++;
            }

            if (!(j < cutoffs.size())) break;
        }

        // if the accumulated personal production exceeds the highest
        // configured advisor personal threshold, take the largest
        for (; j < cutoffs.size(); j++) {
            appliedCommissionRates.add(personalCommConfigEntries.get(personalCommConfigEntries.size() - 1).getC());
        }


        BigDecimal personalComm = new BigDecimal(0);
        for (int i = 0; i < differences.size(); i++) {
            personalComm = personalComm.add(differences.get(i).multiply(appliedCommissionRates.get(i)).divide(HUNDRED, RoundingMode.HALF_UP));
        }

        return new AdvisorProdCommPair(personalProd, personalComm);
    }

    /**
     * True if the personal commission criteria has been met
     * i.e. if the advisorProdThisMonth has exceeded at least the threshold
     *
     * @param agentCode
     * @param year
     * @param month
     * @return Whether eligible for overriding
     */
    public boolean eligibleForOverriding(String agentCode, int year, int month) throws IllegalArgumentException {
        // will throw IllegalStateException if no entries configured
        BigDecimal threshold = ifmConfigService.getLevelThreshold(year, month);
        BigDecimal advisorProdThisMonth = agentCommissionService.getAccumulatedPersonalProd(agentCode, year, month);

        return (advisorProdThisMonth.compareTo(threshold) > -1);
    }

    /**
     * The number of first level subtrees from the particular agent
     * which has ANY member reach 100,000
     * <p>
     * Used to determine the number of accessible overriding levels
     *
     * @param agentCode
     * @param year
     * @param month
     * @return
     */
    public Integer getOverridingLines(String agentCode, int year, int month) {
        TreeLookupService.ReportingTree reportingTree = treeLookupService.getReportingTree(year, month);
        // 100,000
        BigDecimal threshold = ifmConfigService.getLevelThreshold(year, month);
        int lines = 0;
        Map<String, List<String>> agentCodeMap = reportingTree.agentCodeMap;
        if (!agentCodeMap.containsKey(agentCode)) return null; // N/A

        SubtreeIsLineChecker checker = new SubtreeIsLineChecker() {
            @Override
            public boolean check(String _agentCode) {
                BigDecimal accumulatedProd = agentCommissionService.getAccumulatedPersonalProd(_agentCode, year, month);
                if (accumulatedProd.compareTo(threshold) > -1) return true;
                if (agentCodeMap.containsKey(_agentCode)) {
                    for (String child : agentCodeMap.get(_agentCode)) {
                        if (check(child)) return true;
                    }
                }
                return false;
            }
        };

        for (String child : agentCodeMap.get(agentCode)) {
            if (checker.check(child)) lines++;
        }

        return lines;
    }

    /**
     * Gets the levels UP TO the given year & month in that year, REGARDLESS of whether the agent's personal production has reached the threshold
     *
     * @param agentCode
     * @param year
     * @param month
     * @return
     * @throws IOException
     * @throws IllegalStateException
     */
    public Map<Integer, LevelInfo> getLevelInfo(String agentCode, int year, int month) throws IOException, IllegalStateException {
        TreeLookupService.ReportingTree reportingTree = treeLookupService.getReportingTree(year, month);

        // used in the rec
        Map<String, List<String>> agentCodeMap = reportingTree.agentCodeMap;

        Map<Integer, LevelInfo> res = new HashMap<>();
        if (!reportingTree.agentCodeMap.containsKey(agentCode))
            return res;

        if (!reportingTree.agentInfoMap.containsKey(agentCode))
            throw new IllegalArgumentException("Agent code doesn't exist in Agent table.");

        // maximum level to ~~terminate recursion~~
//        int maxLevel = advisorSubCommission.keySet().stream().mapToInt(v -> v).max().orElseThrow(NoSuchElementException::new);

        int _maxLevel = getOverridingLines(agentCode, year, month) + 1;
        LOG.info(String.format("%s %d", agentCode, _maxLevel));

        Map<Integer, BigDecimal> advisorSubCommission = ifmConfigService.getAdvisorSubCommission(year, month, _maxLevel);
        // say you have 4 lines -> 5 levels, but there are only 4 levels
        // actually configured
        int maxLevel = Math.min(advisorSubCommission.size(), _maxLevel);

        if (advisorSubCommission.entrySet().size() == 0) throw new IllegalStateException("No levels given");


        // 100,000
        BigDecimal threshold = ifmConfigService.getLevelThreshold(year, month);

        Map<Integer, List<AgentProdPair>> levelToAdvisorMap = new HashMap<>();

        LevelPopulator levelPopulator = new LevelPopulator() {
            @Override
            public void populate(String _agentCode, int _depth) {

                BigDecimal accumulatedProd = agentCommissionService.getAccumulatedPersonalProd(_agentCode, year, month);

//                if (_depth > maxLevel) return;

                if (advisorSubCommission.containsKey(_depth) && !agentCode.equals(_agentCode)) {
                    if (!levelToAdvisorMap.containsKey(_depth)) levelToAdvisorMap.put(_depth, new ArrayList<>());
                    levelToAdvisorMap.get(_depth).add(new AgentProdPair(_agentCode, accumulatedProd));
                }

                // accumulatedProd.compareTo(threshold) is ALWAYS going to be true
                // if the agentCode is eligible for overriding.
                // _depth == 0 is here for the testing endpoint.
                if ((_depth == 0 ||
                        accumulatedProd.compareTo(threshold) > -1)
                        && _depth < maxLevel
                ) {
                    _depth += 1;
                }

                // if child node
                if (!agentCodeMap.containsKey(_agentCode)) return;
                for (String childAgentCode : agentCodeMap.get(_agentCode)) {
                    populate(childAgentCode, _depth);
                }
            }
        };

        levelPopulator.populate(agentCode, 0);
        levelToAdvisorMap.remove(0); // 0 is not going to be in the config, but remove just in case
        for (int level : levelToAdvisorMap.keySet()) {
            Map<String, BigDecimal> subAdvisors = new HashMap<>();
            for (AgentProdPair pair : levelToAdvisorMap.get(level)) {
                subAdvisors.put(pair.agentCode, pair.prod);
            }
            // get comm rate
            BigDecimal commRate = advisorSubCommission.get(level);
            LevelInfo levelInfo = new LevelInfo(level, subAdvisors, commRate);
            res.put(level, levelInfo);
        }
        return res;
    }

}
