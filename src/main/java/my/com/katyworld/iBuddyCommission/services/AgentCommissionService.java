package my.com.katyworld.iBuddyCommission.services;

import my.com.katyworld.iBuddyCommission.entities.AdvisorProcessingCutoffEntry;
import my.com.katyworld.iBuddyCommission.entities.CmpProdAgtEntry;
import my.com.katyworld.iBuddyCommission.repositories.AdvisorProcessingCutoffsRepository;
import my.com.katyworld.iBuddyCommission.repositories.CmpProdAgtRepository;
import my.com.katyworld.iBuddyCommission.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class AgentCommissionService {

    private static final Logger LOG = Logger.getLogger(AdvisorCalculationService.class.getName());

    @Autowired
    private CmpProdAgtRepository cmpProdAgtRepository;


    /**
     * The actual records in that month (and hence policy numbers involved)
     *
     * @param agentCode
     * @param year
     * @param month
     * @return
     */
    public List<CmpProdAgtEntry> getAdvisorProdRecords(String agentCode, int year, int month) {
        return cmpProdAgtRepository.getAdvisorProdRecords(agentCode, year + String.format("%02d", month));
    }

    /**
     * Gets the production of the particular advisor in the year/month combo
     * Should be used to get the advisor's own personal prod for personal
     * comm calculation also
     *
     * @param agentCode
     * @param year
     * @param month
     * @return
     */
    public BigDecimal getPersonalProd(String agentCode, int year, int month) {
        return cmpProdAgtRepository.getAdvisorProd(agentCode, String.valueOf(year) + String.format("%02d", month));
    }

    /**
     * Sum of the production values in all months in the given year
     *
     * @param agentCode
     * @param year
     * @return total sum
     */
    public BigDecimal getAccumulatedPersonalProd(String agentCode, int year) {
        BigDecimal res = cmpProdAgtRepository.getAccumAdvisorProd(agentCode, String.valueOf(year));
        if (res == null) return BigDecimal.ZERO;
        return res;
    }

    /**
     * Sum of the production values in the given year UP TIL the given month
     *
     * @param agentCode
     * @param year
     * @param month     UP TO the given month (12 if december)
     * @return
     */
    public BigDecimal getAccumulatedPersonalProd(String agentCode, int year, int month) {
        if (month > 12 || month < 1) throw new IllegalArgumentException("Year must be between 1 and 12");
        BigDecimal res = new BigDecimal(0);
        for (int i = 1; i <= month; i++) {
            String twoDigit = String.format("%02d", i);
            BigDecimal thisMonth = cmpProdAgtRepository.getAccumAdvisorProd(agentCode, String.format("%d%s", year, twoDigit));
            res = res.add(thisMonth == null ? new BigDecimal(0) : thisMonth);
        }

        return res;
    }


    /**
     * Production values in the given year UP TIL the given month
     *
     * @param agentCode
     * @param year
     * @param month     UP TO the given month (12 if december)
     * @return
     */
    public List<BigDecimal> getAccumulatedAdvisorProdRecords(String agentCode, int year, int month) {
        if (month > 12 || month < 1) throw new IllegalArgumentException("Year must be between 1 and 12");
        List<BigDecimal> res = new ArrayList<>();
        for (int i = 1; i <= month; i++) {
            String twoDigit = String.format("%02d", i);
            BigDecimal thisMonth = cmpProdAgtRepository.getAccumAdvisorProd(agentCode, String.format("%d%s", year, twoDigit));
            res.add(thisMonth == null ? new BigDecimal(0) : thisMonth);
        }

        return res;
    }


    @Autowired
    private AdvisorProcessingCutoffsRepository advisorProcessingCutoffsRepository;

    /**
     * Gets all cutoffs in the year of the given date which are before
     * the given date
     *
     * @param advisorCode
     * @param d
     * @return
     */
    public List<AdvisorProcessingCutoffEntry> getLastCutoffs(String advisorCode, Date d) {
        Date date = d != null ? d : new Date();
        List<AdvisorProcessingCutoffEntry> l = advisorProcessingCutoffsRepository.getLastCutoffs(advisorCode, date);
        Assert.state(Util.isStrictlyNonDecreasing(l.stream().map(e -> e.accumProd).collect(Collectors.toList())), "Cutoffs are strictly non-decreasing");
        return l;
    }

}
