package my.com.katyworld.iBuddyCommission.services;

import my.com.katyworld.iBuddyCommission.entities.AdvisorProcessingCutoffEntry;
import my.com.katyworld.iBuddyCommission.entities.Agent;
import my.com.katyworld.iBuddyCommission.entities.CommissionSummaryEntry;
import my.com.katyworld.iBuddyCommission.entities.CommissionTOVBreakDown;
import my.com.katyworld.iBuddyCommission.repositories.AdvisorProcessingCutoffsRepository;
import my.com.katyworld.iBuddyCommission.repositories.CommissionSummaryRepository;
import my.com.katyworld.iBuddyCommission.repositories.CommissionTOVBreakDownRepository;
import my.com.katyworld.iBuddyCommission.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * For writing the resulting values into the CommissionSummary and breakdown tables
 */
@Service
public class ProcessingService {

    private static final Logger LOG = Logger.getLogger(AdvisorCalculationService.class.getName());

    @Autowired
    private CommissionSummaryRepository commissionSummaryRepository;

    @Autowired
    private CommissionTOVBreakDownRepository commissionTOVBreakDownRepository;

    @Autowired
    private TreeLookupService treeLookupService;

    @Autowired
    private AdvisorCalculationService advisorCalculationService;

    @Autowired
    private AgentCommissionService agentCommissionService;

    @Autowired
    private AdvisorProcessingCutoffsRepository advisorProcessingCutoffsRepository;

    public void deleteCommSummary(String agentCode, int currentYear, int currentMonth) {
        String prodMonth = String.format("%d%s", currentYear, String.format("%02d", currentMonth));
        commissionSummaryRepository.deleteByFvAgentCodeAndProdMonth(agentCode, prodMonth);
        commissionSummaryRepository.flush();
    }

    private void writeCommSummary(String agentCode, AdvisorCalculationService.AdvisorSalesComm a, LocalDate currentDate) throws IOException {
        int currentYear = currentDate.getYear();
        int currentMonth = currentDate.getMonth().getValue();

        deleteCommSummary(agentCode, currentYear, currentMonth);

        CommissionSummaryEntry fycSummaryEntry = new CommissionSummaryEntry();
        String prodMonth = String.format("%d%s", currentYear, String.format("%02d", currentMonth));
        fycSummaryEntry.prodMonth = prodMonth;
        fycSummaryEntry.commissionDate = Date.from(currentDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        fycSummaryEntry.fvAgentCode = agentCode;
        fycSummaryEntry.benCode = "FYC";
        fycSummaryEntry.description = "Personal Commission";
        fycSummaryEntry.toShow = true;
        fycSummaryEntry.toCalc = true;
        fycSummaryEntry.creditAmt = a.personalComm;
        commissionSummaryRepository.save(fycSummaryEntry);

        if (a.levelInfo == null) return;
        for (Map.Entry<Integer, AdvisorCalculationService.LevelInfo> g : a.levelInfo.entrySet()) {
            AdvisorCalculationService.LevelInfo v = g.getValue();
            CommissionSummaryEntry tovSummaryEntry = new CommissionSummaryEntry();
            tovSummaryEntry.prodMonth = prodMonth;
            tovSummaryEntry.commissionDate = Date.from(currentDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
            tovSummaryEntry.fvAgentCode = agentCode;
            tovSummaryEntry.benCode = String.format("%s%d", "TOVL", v.level);
            tovSummaryEntry.description = String.format("%s %d", "Overriding Commission Level", v.level);
            tovSummaryEntry.toShow = true;
            tovSummaryEntry.toCalc = true;
            tovSummaryEntry.creditAmt = v.comm;
            commissionSummaryRepository.save(tovSummaryEntry);
        }
    }

    public void deleteCommTOVBreakDown(String agentCode, int currentYear, int currentMonth) {
        String prodMonth = String.format("%d%s", currentYear, String.format("%02d", currentMonth));
        commissionTOVBreakDownRepository.deleteByFvAgentCodeAndProdMonth(agentCode, prodMonth);
        commissionTOVBreakDownRepository.flush();
    }

    private void writeCommTOVBreakDown(String agentCode, AdvisorCalculationService.AdvisorSalesComm a, LocalDate currentDate) throws IOException {
        int currentYear = currentDate.getYear();
        int currentMonth = currentDate.getMonth().getValue();
        String prodMonth = String.format("%d%s", currentYear, String.format("%02d", currentMonth));

        deleteCommTOVBreakDown(agentCode, currentYear, currentMonth);

        if (a.levelInfo == null) return;

        for (Map.Entry<Integer, AdvisorCalculationService.LevelInfo> g : a.levelInfo.entrySet()) {
            for (Map.Entry<String, BigDecimal> subAdvisor : g.getValue().subAdvisors.entrySet()) {
                CommissionTOVBreakDown commissionTOVBreakDown = new CommissionTOVBreakDown();
                commissionTOVBreakDown.prodMonth = prodMonth;
                commissionTOVBreakDown.fvAgentCode = agentCode;
                commissionTOVBreakDown.fvBACode = subAdvisor.getKey();
                commissionTOVBreakDown.benCode = String.format("%s%d", "TOVL", g.getKey());
                commissionTOVBreakDown.creditAmt = subAdvisor.getValue();
                commissionTOVBreakDownRepository.save(commissionTOVBreakDown);
            }
        }
    }

    /**
     * Performs calculation for EVERY SINGLE ADVISOR, writes the results to db
     * updates AdvisorProcessingCutoffs also
     */
    @Scheduled(cron = "${ifm-process.cron}")
    public void PROCESS() throws IOException {
        LocalDate currentDate = LocalDate.now();
        int currentYear = currentDate.getYear();
        int currentMonth = currentDate.getMonth().getValue();

        List<Agent> agents = treeLookupService.findAll();
        for (Agent a : agents) {
            if (!a.AgentType.startsWith(Constants.AgentTypes.ADVISOR)) continue;
            // testing
//            if (!a.AgentCode.equals("SLD10009")) continue;

            BigDecimal accumulatedPersonalProd = agentCommissionService.getAccumulatedPersonalProd(a.AgentCode, currentYear);

            // write into AdvisorProcessingCutoffs so it is included in
            // the commission calculation
            AdvisorProcessingCutoffEntry advisorProcessingCutoffEntry = new AdvisorProcessingCutoffEntry();
            advisorProcessingCutoffEntry.accumProd = accumulatedPersonalProd;
            advisorProcessingCutoffEntry.advisorCode = a.AgentCode;

            advisorProcessingCutoffsRepository.save(advisorProcessingCutoffEntry);

            AdvisorCalculationService.AdvisorSalesComm b = advisorCalculationService.getAdvisorSalesComm(a.AgentCode, currentYear, currentMonth);
            writeCommSummary(a.AgentCode, b, currentDate);
            writeCommTOVBreakDown(a.AgentCode, b, currentDate);
        }
    }

}
