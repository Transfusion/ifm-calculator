package my.com.katyworld.iBuddyCommission.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "Cmp_Prod_Agt")
public class CmpProdAgtEntry {
    @Id
    @Column(name = "ProdAgtId", columnDefinition = "bigint")
    public int ProdAgtId;

    @Column(name = "CarrierCode", columnDefinition = "varchar(2)")
    public String CarrierCode;

    @Column(name = "BizSrc", columnDefinition = "varchar(10)")
    public String BizSrc;

    @Column(name = "CycleMth", columnDefinition = "int")
    public String CycleMth;

    @Column(name = "Curr_APEI", columnDefinition = "money")
    public BigDecimal Curr_APEI;

    @Column(name = "Serv_AgentType", columnDefinition = "varchar(2)")
    public String Serv_AgentType;

    @Column(name = "Serv_AgentCode", columnDefinition = "varchar(12)")
    public String Serv_AgentCode;
}
