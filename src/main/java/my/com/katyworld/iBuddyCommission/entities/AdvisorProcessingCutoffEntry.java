package my.com.katyworld.iBuddyCommission.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "AdvisorProcessingCutoffs")
public class AdvisorProcessingCutoffEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", columnDefinition = "bigint")
    public int id;

    @Column(name = "accumProd", columnDefinition = "decimal(18,4)")
    public BigDecimal accumProd;

    @Column(name = "advisorCode", columnDefinition = "varchar(30)")
    public String advisorCode;

    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated", columnDefinition = "datetime", insertable = false, updatable = false)
    public Date updated;

}
