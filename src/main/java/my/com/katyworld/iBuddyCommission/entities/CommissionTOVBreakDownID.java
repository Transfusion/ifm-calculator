package my.com.katyworld.iBuddyCommission.entities;

import java.io.Serializable;
import java.util.Objects;

public class CommissionTOVBreakDownID implements Serializable {
    private String prodMonth;
    private String fvAgentCode;
    private String fvBACode;

    public CommissionTOVBreakDownID() {
    }

    public CommissionTOVBreakDownID(String prodMonth, String FVAgentCode, String FVBACode) {
        this.prodMonth = prodMonth;
        this.fvAgentCode = FVAgentCode;
        this.fvBACode = FVBACode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CommissionTOVBreakDownID id = (CommissionTOVBreakDownID) o;
        return prodMonth.equals(id.prodMonth) &&
                fvAgentCode.equals(id.fvAgentCode) &&
                fvBACode.equals(id.fvBACode);
    }


    @Override
    public int hashCode() {
        return Objects.hash(prodMonth, fvAgentCode, fvBACode);
    }
}
