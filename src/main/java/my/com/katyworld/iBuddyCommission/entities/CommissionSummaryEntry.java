package my.com.katyworld.iBuddyCommission.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "tbl_CommissionSummary")
public class CommissionSummaryEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CommID", columnDefinition = "bigint")
    public int commID;

    @Column(name = "ProdMonth", columnDefinition = "varchar(6)")
    public String prodMonth;

    @Column(name = "CommissionDate", columnDefinition = "date")
    public Date commissionDate;

    @Column(name = "FVAgentCode", columnDefinition = "varchar(10)")
    public String fvAgentCode;

    @Column(name = "BenCode", columnDefinition = "varchar(10)")
    public String benCode;

    @Column(name = "toShow", columnDefinition = "bit")
    public Boolean toShow;

    @Column(name = "toCalc", columnDefinition = "bit")
    public Boolean toCalc;

    @Column(name = "CreditAmt", columnDefinition = "money")
    public BigDecimal creditAmt;

    @Column(name = "Description", columnDefinition = "varchar(255)")
    public String description;
}
