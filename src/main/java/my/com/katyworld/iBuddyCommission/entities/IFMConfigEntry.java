package my.com.katyworld.iBuddyCommission.entities;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import com.vladmihalcea.hibernate.type.json.internal.JacksonUtil;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.Date;

@TypeDef(
        name = "json",
        typeClass = JsonStringType.class
)

@Entity
@Table(name = "IFMConfig")
public class IFMConfigEntry {
    @Id
    @Column(name = "id", columnDefinition = "bigint")
    public int id;

    @Column(name = "k", columnDefinition = "nvarchar(50)")
    public String k;

    @Type(type = "json")
    @Column(name = "v", columnDefinition = "nvarchar(max)")
    public String v;

    @Temporal(TemporalType.DATE)
    @Column(name = "updated", columnDefinition = "datetime")
    public Date updated;


    public JsonNode getJsonNodeValue() {
        return JacksonUtil.toJsonNode(v);
    }

}
