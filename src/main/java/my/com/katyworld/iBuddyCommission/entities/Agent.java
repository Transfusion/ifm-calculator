package my.com.katyworld.iBuddyCommission.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "Agent")
public class Agent implements Serializable {
    @Column(name = "CarrierCode", columnDefinition = "varchar(2)")
    public String CarrierCode;

    @Id
    @Column(name = "AgentCode", columnDefinition = "varchar(10)")
    public String AgentCode;

    @Column(name = "DirectAgtCode", columnDefinition = "varchar(10)")
    public String DirectAgtCode;

    @Column(name = "AgentType", columnDefinition = "varchar(2)")
    public String AgentType;

    @Override
    public String toString() {
        return this.AgentCode + " " + this.DirectAgtCode;
    }
}
