package my.com.katyworld.iBuddyCommission.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * fields are lowercase because deleting must be supported
 */
@Entity
@IdClass(CommissionTOVBreakDownID.class)
@Table(name = "tbl_CommissionTOVBreakDown")
public class CommissionTOVBreakDown implements Serializable {
    @Id
    @Column(name = "ProdMonth", columnDefinition = "varchar(6)")
    public String prodMonth;

    @Id
    @Column(name = "FVAgentCode", columnDefinition = "varchar(10)")
    public String fvAgentCode;

    @Id
    @Column(name = "FVBACode", columnDefinition = "varchar(10)")
    public String fvBACode;

    @Column(name = "BenCode", columnDefinition = "varchar(10)")
    public String benCode;

    @Column(name = "CreditAmt", columnDefinition = "money")
    public BigDecimal creditAmt;


}
