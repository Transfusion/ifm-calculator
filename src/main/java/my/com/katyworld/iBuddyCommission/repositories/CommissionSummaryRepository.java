package my.com.katyworld.iBuddyCommission.repositories;

import my.com.katyworld.iBuddyCommission.entities.CommissionSummaryEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;


public interface CommissionSummaryRepository extends JpaRepository<CommissionSummaryEntry, Integer> {

    @Modifying
    @Transactional
    long deleteByFvAgentCodeAndProdMonth(String fvAgentCode, String prodMonth);
}
