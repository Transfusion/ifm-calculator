package my.com.katyworld.iBuddyCommission.repositories;

import my.com.katyworld.iBuddyCommission.entities.IFMConfigEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface IFMConfigRepository extends JpaRepository<IFMConfigEntry, String> {
//    IFMConfigEntry findIFMConfigEntryByK(String key);

    /**
     * gets the latest config entry for that key which was created
     * before the given date
     *
     * @param k        key
     * @param yyyymmdd e.g. 2020-02-29
     * @return the config entry, null if doesn't exist
     */
    @Query("select i from IFMConfigEntry i where k = :k and updated <= convert(datetime, :yyyymmdd ) order by updated desc ")
    List<IFMConfigEntry> getLatestIFMConfigEntryByK(@Param("k") String k,
                                                     @Param("yyyymmdd") String yyyymmdd);

}
