package my.com.katyworld.iBuddyCommission.repositories;

import my.com.katyworld.iBuddyCommission.entities.CmpProdAgtEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface CmpProdAgtRepository extends JpaRepository<CmpProdAgtEntry, String> {

    @Query("SELECT c FROM CmpProdAgtEntry c WHERE CarrierCode = 'FM' and CycleMth = :yearMonth and Serv_AgentCode = :agentCode ")
    List<CmpProdAgtEntry> getAdvisorProdRecords(@Param("agentCode") String agentCode,
                                                @Param("yearMonth") String yearMonth);

    @Query("SELECT SUM(Curr_APEI) FROM CmpProdAgtEntry WHERE CarrierCode = 'FM' and CycleMth = :yearMonth and Serv_AgentCode = :agentCode ")
    BigDecimal getAdvisorProd(@Param("agentCode") String agentCode,
                              @Param("yearMonth") String yearMonth);


    @Query("SELECT SUM(Curr_APEI) FROM CmpProdAgtEntry WHERE CarrierCode = 'FM' and CycleMth LIKE :cycleMth% and Serv_AgentCode = :agentCode ")
    BigDecimal getAccumAdvisorProd(@Param("agentCode") String agentCode,
                                   @Param("cycleMth") String cycleMth);
}