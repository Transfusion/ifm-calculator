package my.com.katyworld.iBuddyCommission.repositories;

import my.com.katyworld.iBuddyCommission.entities.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentRepository extends JpaRepository<Agent, String> {
}
