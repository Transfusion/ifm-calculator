package my.com.katyworld.iBuddyCommission.repositories;

import my.com.katyworld.iBuddyCommission.entities.CommissionTOVBreakDown;
import my.com.katyworld.iBuddyCommission.entities.CommissionTOVBreakDownID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import javax.transaction.Transactional;

public interface CommissionTOVBreakDownRepository extends JpaRepository<CommissionTOVBreakDown, CommissionTOVBreakDownID> {
    @Modifying
    @Transactional
    long deleteByFvAgentCodeAndProdMonth(String fvAgentCode, String prodMonth);
}
