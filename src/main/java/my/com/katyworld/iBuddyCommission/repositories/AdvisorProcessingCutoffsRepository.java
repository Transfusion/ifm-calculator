package my.com.katyworld.iBuddyCommission.repositories;

import my.com.katyworld.iBuddyCommission.entities.AdvisorProcessingCutoffEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface AdvisorProcessingCutoffsRepository extends JpaRepository<AdvisorProcessingCutoffEntry, Integer> {

//    AdvisorProcessingCutoffEntry findFirstByAdvisorCodeAndUpdatedBeforeOrderByUpdatedDesc(String advisorCode, Date updated);

    @Query("select c from AdvisorProcessingCutoffEntry c where advisorCode = :advisorCode and YEAR(updated) = YEAR(:d) and updated <= :d order by updated asc")
    List<AdvisorProcessingCutoffEntry> getLastCutoffs(String advisorCode, Date d);
}
