package my.com.katyworld.iBuddyCommission.util;

import java.math.BigDecimal;
import java.time.LocalDate;

import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

import java.time.temporal.TemporalAdjusters;
import java.util.List;

public class Util {
    public static int getLastDayOfMonth(int year, int month) {
        LocalDate ld = LocalDate.parse(String.format("%d-%02d-01", year, month));
        return ld.with(lastDayOfMonth()).getDayOfMonth();
    }

    public static boolean isStrictlyNonDecreasing(List<BigDecimal> l) {
        for (int i = 1; i < l.size(); i++) {
            if (l.get(i).compareTo(l.get(i - 1)) < 0) return false;
        }
        return true;
    }
}
