package my.com.katyworld.iBuddyCommission.util;

public class Constants {
    public final class AgentTypes {
        public static final String ADVISOR = "A";
        public static final String IBUDDY = "B";
    }

    public final class IFMConfigIDs {
        public static final String ADVISOR_COMMISSION = "advisorCommission";
    }
}
