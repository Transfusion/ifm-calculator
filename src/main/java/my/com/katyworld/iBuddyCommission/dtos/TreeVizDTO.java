package my.com.katyworld.iBuddyCommission.dtos;

import my.com.katyworld.iBuddyCommission.entities.Agent;
import my.com.katyworld.iBuddyCommission.services.TreeLookupService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeVizDTO {
    public final String name;
    public final Map<String, String> attributes = new HashMap<>();
    public final List<TreeVizDTO> children = new ArrayList<>();

    public TreeVizDTO(String root, TreeLookupService.ReportingTree reportingTree) {
        this.name = root;

        Agent agt = reportingTree.agentInfoMap.get(root);
        Map<String, List<String>> agentCodeMap = reportingTree.agentCodeMap;

        // populate attributes at this node
//        TreeLookupService.AgentInfo agentInfo = agentInfoMap.get(this.name);
//        if (agentInfo.getAFYP() != null) {
//        attributes.put("afyp", agentInfo.getAFYP().toString());
//        }
//        attributes.put("persProd", agentInfo.getPersonalProduction().toString());
//        attributes.put("persProdComm", agentInfo.getPersonalProdCommission().toString());
        attributes.put("agentType", agt.AgentType);

        /*if (agentInfo instanceof TreeLookupService.AdvisorAgentInfo) {
            TreeLookupService.AdvisorAgentInfo advisorAgentInfo = (TreeLookupService.AdvisorAgentInfo) agentInfo;
            attributes.put("totalAdCom", advisorAgentInfo.getAdvisorComm().toString());
            attributes.put("acb", advisorAgentInfo.getAdvisorCommBreakdown().toString());
        }

        if (agentInfo instanceof TreeLookupService.iBuddyAgentInfo) {
            TreeLookupService.iBuddyAgentInfo iBuddyAgentInfo = (TreeLookupService.iBuddyAgentInfo) agentInfo;
            attributes.put("iBuddyComm", iBuddyAgentInfo.getiBuddyComm().toString());
        }*/

        if (agentCodeMap.containsKey(root)) {
            for (String child : agentCodeMap.get(root)) {
                this.children.add(new TreeVizDTO(child, reportingTree));
            }
        }
    }
}
