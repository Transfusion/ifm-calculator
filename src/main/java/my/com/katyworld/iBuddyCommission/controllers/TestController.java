package my.com.katyworld.iBuddyCommission.controllers;

import io.swagger.v3.oas.annotations.tags.Tag;
import my.com.katyworld.iBuddyCommission.dtos.TreeVizDTO;
import my.com.katyworld.iBuddyCommission.entities.Agent;
import my.com.katyworld.iBuddyCommission.services.TreeLookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/v1/test")
@Tag(name = "TestController", description = "Testing Tree Visualization")
public class TestController {
    private static final Logger LOG = Logger.getLogger(TestController.class.getName());

    @Autowired
    private TreeLookupService treeLookupService;

    @GetMapping("/dummy")
    public Object getDummyObject() {
        return new Object() {
            public final String test = "test";
            public final Integer num = 1;
        };
    }

    @GetMapping("/getAllAgents")
    public List<Agent> getAllAgents() {
        return treeLookupService.findAll();
    }

    @GetMapping("/visualizeTrees")
    public List<TreeVizDTO> visualizeTrees(Optional<Integer> year, Optional<Integer> month) {
        if (month.isEmpty() || year.isEmpty()) {
            LocalDate currentDate = LocalDate.now();
            return treeLookupService.getTreeVizDTOs(currentDate.getYear(),
                    currentDate.getMonth().getValue());
        }

        return treeLookupService.getTreeVizDTOs(year.get(), month.get());
    }

//    @PostMapping(path = "/updatePersonalProduction", consumes = "application/json", produces = "application/json")
//    public Object updatePersonalProduction(@RequestBody Map<String, List<BigDecimal>> ppMap) {
//        LOG.log(Level.INFO, "Received ppMap: " + ppMap.toString());
//        treeLookupService.updatePersonalProduction(ppMap);
//        return new Object() {
//            public final boolean success = true;
//        };
//    }
}
