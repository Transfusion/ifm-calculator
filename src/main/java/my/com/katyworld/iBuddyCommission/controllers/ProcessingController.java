package my.com.katyworld.iBuddyCommission.controllers;

import my.com.katyworld.iBuddyCommission.services.ProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/process")
public class ProcessingController {
    @Autowired
    private ProcessingService processingService;

    @GetMapping("/perform")
    public void performProcess() throws IOException {
        processingService.PROCESS();
    }

    @DeleteMapping("/deleteCommSummary")
    public void deleteCommSummary(String agentCode, int currentYear, int currentMonth) {
        processingService.deleteCommSummary(agentCode, currentYear, currentMonth);
    }

    @DeleteMapping("/deleteCommTOVBreakDown")
    public void deleteCommTOVBreakDown(String agentCode, int currentYear, int currentMonth) {
        processingService.deleteCommTOVBreakDown(agentCode, currentYear, currentMonth);
    }
}
