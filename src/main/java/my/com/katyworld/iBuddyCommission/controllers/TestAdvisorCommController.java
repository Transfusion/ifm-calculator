package my.com.katyworld.iBuddyCommission.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import my.com.katyworld.iBuddyCommission.dtos.TreeVizDTO;
import my.com.katyworld.iBuddyCommission.entities.AdvisorProcessingCutoffEntry;
import my.com.katyworld.iBuddyCommission.services.AdvisorCalculationService;
import my.com.katyworld.iBuddyCommission.services.AgentCommissionService;
import my.com.katyworld.iBuddyCommission.services.TreeLookupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/v1/testadcomm")
@Tag(name = "TestAdvisorCommController", description = "Testing Advisor Commission")
public class TestAdvisorCommController {
    private static final Logger LOG = Logger.getLogger(TestController.class.getName());

    @Autowired
    AgentCommissionService agentCommissionService;

    @Autowired
    AdvisorCalculationService advisorCalculationService;

    @Autowired
    TreeLookupService treeLookupService;

    @Operation(summary = "For debugging purposes. Gets the calculated personal prod & comm and overriding tree if eligible in the given year & month.")
    @GetMapping("/getAdvisorSalesComm")
    public AdvisorCalculationService.AdvisorSalesComm getAdvisorSalesComm(String agentCode, Optional<Integer> year, Optional<Integer> month) throws IOException {
        if (month.isEmpty() || year.isEmpty()) {
            LocalDate currentDate = LocalDate.now();
            return advisorCalculationService.getAdvisorSalesComm(agentCode, currentDate.getYear(), currentDate.getMonth().getValue());
        }
        return advisorCalculationService.getAdvisorSalesComm(agentCode, year.get(), month.get());
    }


    @Operation(summary = "For debugging purposes. Gets the prod and comm in the given year for the given advisor UP TO and including the given month")
    @GetMapping("/getAdvisorProdComm")
    public AdvisorCalculationService.AdvisorProdCommPair getAdvisorProdComm(String agentCode, Optional<Integer> year, Optional<Integer> month) throws IOException {
        if (month.isEmpty() || year.isEmpty()) {
            LocalDate currentDate = LocalDate.now();
            return advisorCalculationService.getAdvisorProdComm(agentCode, currentDate.getYear(), currentDate.getMonth().getValue());
        }
        return advisorCalculationService.getAdvisorProdComm(agentCode, year.get(), month.get());
    }

    @Operation(summary = "For debugging purposes. Gets the overriding tree of the given advisor.", description = "Does not check whether the advisor is actually eligible for overriding.")
    @GetMapping("/getLevelInfo")
    public Map<Integer, AdvisorCalculationService.LevelInfo> getLevelInfo(String agentCode, Optional<Integer> year, Optional<Integer> month) throws IOException {
        if (month.isEmpty() || year.isEmpty()) {
            LocalDate currentDate = LocalDate.now();
            return advisorCalculationService.getLevelInfo(agentCode, currentDate.getYear(), currentDate.getMonth().getValue());
        }
        return advisorCalculationService.getLevelInfo(agentCode, year.get(), month.get());
    }


    @Operation(summary = "For debugging purposes. Gets the total production of the given advisor ONLY in the given year & month.", description = "Current year and month if either is not provided.")
    @GetMapping("/getAdvisorProd")
    public BigDecimal getAdvisorProd(String agentCode, Optional<Integer> year, Optional<Integer> month) {

        if (month.isEmpty() || year.isEmpty()) {
            LocalDate currentDate = LocalDate.now();
            return agentCommissionService.getPersonalProd(agentCode, currentDate.getYear(), currentDate.getMonth().getValue());
        }
        return agentCommissionService.getPersonalProd(agentCode, year.get(), month.get());
    }


    @Operation(summary = "For debugging purposes. Gets the total personal prod ONLY for the given advisor in the given year, UP TO and including the given month.", description = "Returns the sum of the entire year if month is not provided. Year is the current year if not provided.")
    @GetMapping("/getAccumulatedAdvisorProd")
    public BigDecimal getAccumulatedAdvisorProd(String agentCode, Optional<Integer> year, Optional<Integer> month) {

        if (year.isEmpty()) {
            LocalDate currentDate = LocalDate.now();
            return agentCommissionService.getAccumulatedPersonalProd(agentCode, currentDate.getYear());
        }

        if (month.isEmpty()) {
            return agentCommissionService.getAccumulatedPersonalProd(agentCode, year.get());
        }
        return agentCommissionService.getAccumulatedPersonalProd(agentCode, year.get(), month.get());
    }

    @Operation(summary = "For debugging purposes. Gets all cutoffs in the year of the given date which are before the given date")
    @GetMapping("/getLastCutoffs")
    public List<AdvisorProcessingCutoffEntry> getLastCutoffs(String advisorCode, @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Optional<Date> d) {
        Date date = new Date();
        if (d.isEmpty()) {
            return agentCommissionService.getLastCutoffs(advisorCode, date);
        } else {
            LOG.info("parsed date " + d.get().toString());
            return agentCommissionService.getLastCutoffs(advisorCode, d.get());
        }
    }


    @GetMapping("/visualizeAdvisorComm")
    public List<TreeVizDTO> visualizeAdvisorComm(String agentCode, Optional<Integer> year, Optional<Integer> month) {
        if (month.isEmpty() || year.isEmpty()) {
            LocalDate currentDate = LocalDate.now();
            return treeLookupService.getAdvisorCommTreeVizDTOs(agentCode, currentDate.getYear(),
                    currentDate.getMonth().getValue());
        }
        return treeLookupService.getAdvisorCommTreeVizDTOs(agentCode, year.get(), month.get());
    }

}
