package my.com.katyworld.iBuddyCommission

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
//import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.scheduling.annotation.EnableScheduling

@EnableCaching
@EnableScheduling
@SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
class IBuddyCommissionApplication


fun main(args: Array<String>) {
    runApplication<IBuddyCommissionApplication>(*args)
}
